import csv
import configparser

def removeLastComma(query):
    if query[-1] == ',':
        query = query[0:-1]
    return query

def generateSQLInsertHeader(tableName):
    return 'INSERT INTO {} VALUES '.format(tableName)

def generateSQLValues(values):
    return '({}),'.format(','.join(['"{}"'.format(str(x)) for x in values]))

def generateSQLFromCSV(filename, tableName, delimiter, parseData=None):
    print('Parsing {} in progress'.format(filename))
    with open(filename, newline='', encoding="utf8") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=delimiter)

        query = generateSQLInsertHeader(tableName)
        index = 0

        for row in spamreader:
            if parseData:
                query += generateSQLValues(parseData(row))
            else:
                query += generateSQLValue(row)

            if index%100 == 0:
                print('.', end='')
            index += 1

        return removeLastComma(query)

globalGenre = dict()
globalGenreId = 0

queryMovieGenre = generateSQLInsertHeader('movie_genre')

globalUsersIdSet = set()

def parseDataMovie(row):
    global globalGenre
    global globalGenreId
    global queryMovieGenre
    list = row[2].split('|')
    for r in list:
        if r not in globalGenre:
            globalGenre[r] = globalGenreId
            globalGenreId += 1

        queryMovieGenre += generateSQLValues([row[0], globalGenre[r]])
    return row[0:2]

def parseDataGetUserId(row):
    global globalUsersIdSet
    globalUsersIdSet.add(row[0])
    return row

def writeSQLToFile(sqlQuery, filename):
    with open(filename, 'w', encoding='utf8') as saveFile:
        saveFile.write(sqlQuery)

def readDataFromConfig(configName):
    config = configparser.ConfigParser()
    status = config.read(configName)
    if configName in status:
        return {'RatingPathCSV': config['CSVFiles']['RatingPath'],
                'TagsPathCSV': config['CSVFiles']['TagsPath'],
                'MoviesPathCSV': config['CSVFiles']['MoviesPath'],
                'RatingPathSQL': config['SQLFiles']['RatingPath'],
                'TagsPathSQL': config['SQLFiles']['TagsPath'],
                'MoviesPathSQL': config['SQLFiles']['MoviesPath'],
                'MoviesGenrePathSQL': config['SQLFiles']['MoviesGenrePath'],
                'GenrePathSQL': config['SQLFiles']['GenrePath'],
                'UserPathSQL': config['SQLFiles']['UserPath']}
    return {'RatingPathCSV': '../../data/ratings.csv',
            'TagsPathCSV': '../../data/tags.csv',
            'MoviesPathCSV': '../../data/movies.csv',
            'RatingPathSQL': '../../data/ratings.sql',
            'TagsPathSQL': '../../data/tags.sql',
            'MoviesPathSQL': '../../data/movies.sql',
            'MoviesGenrePathSQL': '../../data/movie_genre.sql',
            'GenrePathSQL': '../../data/genres.sql',
            'UserPathSQL': '../../data/users.sql'}

def main():
    config = readDataFromConfig("config.ini")

    writeSQLToFile(generateSQLFromCSV(config['RatingPathCSV'], 'rating', ',', parseDataGetUserId), config['RatingPathSQL'])
    writeSQLToFile(generateSQLFromCSV(config['TagsPathCSV'], 'tags', ',', parseDataGetUserId), config['TagsPathSQL'])

    writeSQLToFile(generateSQLFromCSV(config['MoviesPathCSV'], 'movies', ',', parseDataMovie), config['MoviesPathSQL'])
    writeSQLToFile(removeLastComma(queryMovieGenre), config['MoviesGenrePathSQL'])

    queryGenre = generateSQLInsertHeader('genre')
    for key in globalGenre:
        queryGenre += generateSQLValues([globalGenre[key], key]) 
    writeSQLToFile(removeLastComma(queryGenre), config['GenrePathSQL'])

    queryUser = generateSQLInsertHeader('user')
    for userId in globalUsersIdSet:
        queryUser += generateSQLValues([userId, 'John_{}'.format(userId)])
    writeSQLToFile(removeLastComma(queryUser), config['UserPathSQL'])


if __name__ == "__main__":
	main()
