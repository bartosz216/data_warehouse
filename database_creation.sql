create database movies;
create table Genre(genreId int primary key not null UNIQUE, genreName varchar(256) not null);
create table Movie(movieId int primary key not null UNIQUE, title varchar(256) not null);
create table Users(userId int primary key not null UNIQUE, userName varchar(256) not null);
create table Movie_Genre(
  movieGenreId int primary key not null unique,
  FK_movie int not null,
  FK_genre int not null,
  foreign key (FK_movie) references Movie(movieId),
  foreign key (FK_genre) references Genre(genreId)
);
create table Tag(
  tagId int primary key not null unique,
  FK_userTag int not null,
  FK_movieTag int not null,
  tag varchar(256),
  timestamp TIMESTAMP,
  foreign key (FK_movieTag) references Movie(movieId),
  foreign key (FK_userTag) references Users(userId)
);
create table Rating(
  ratingId int primary key not null unique,
  FK_userRating int not null,
  FK_movieRating int not null,
  rating REAL,
  timestamp TIMESTAMP,
  foreign key (FK_movieRating) references Movie(movieId),
  foreign key (FK_userRating) references Users(userId)
);
